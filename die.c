#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "main.h"

void
die(const char *const message, ...)
{
	assert(message != NULL);
	va_list arguments;
	(void)fprintf(stderr, "ERROR: ");
	va_start(arguments, message);
	(void)vfprintf(stderr, message, arguments);
	va_end(arguments);
	(void)fprintf(stderr, "\n");
	exit(EXIT_FAILURE);
}
