#ifndef MAIN_H
#define MAIN_H

#ifndef NEWLINE_LENGTH
#if defined(__unix__) || defined(__APPLE__)
#define NEWLINE_LENGTH 1
#else
#define NEWLINE_LENGTH 2
#endif
#endif /* NEWLINE_LENGTH */

#ifndef NO_RETURN
#ifdef __GNUC__
#define NO_RETURN __attribute__((noreturn))
#else
#define NO_RETURN
#endif
#endif /* NO_RETURN */

#ifndef ALIGNED
#ifdef __GNUC__
#define ALIGNED(SIZE) __attribute__((aligned(SIZE)))
#else
#define ALIGNED(SIZE)
#endif
#endif /* ALIGNED */

void die(const char *message, ...) NO_RETURN;
int is_newline(const char *string);

#endif /* MAIN_H */