#include <assert.h>

#include "parser.h"

void
parser_initialize(struct parser *const parser, char *const code)
{
	assert(parser != NULL && code != NULL);
	parser->lexer.code = parser->lexer.line_begin = code;
	parser->lexer.line = 1;
	parser->token.type = TOKEN_TYPE_NONE;
	parser->token.value = NULL;
}